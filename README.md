# Feathr Frontend Engineer Interview Coding Exercise

Hello potential Feathr candidate!

Today we are going to build an application with simple requirements to showcase your ability to solve problems.

## The Problem

If you're looking to go out and get lunch in downtown Gainsville, there are a lot of options to choose from.

In fact - there may be too many options. Wouldn't it be nice if you could just push a button and have an app tell you where to eat?

## Overview

Today we are building a restaurant suggestion application.

A user should be able to input a few things like (for example):
* Location
* Keywords to match restaurant
* Price range

Based on inputs, the application should display a single potential restaurant.

If you're reading this and thinking "This seems overly simple...", you're right!

The goal of this exercise is to simply see how you'd go about solving a problem with ambiguous/loose requirements. Here at Feathr, our engineers speak directly with customers and interpret those conversations into technical solutions, similar to this exercise.

## Getting Started

Get started by forking this repository.

For the frontend, we are flexible on what you are allowed to use. Feel free to use whatever technology you would like. The only requirement is that you clearly define how to install and run the application via the README.md file in the repository.

That means you can use any technology ranging from React, Vue, Angular, or even plain ol' Javascript. Feel free to use a UI kit that you feel comfortable with to style your page.
We'd just like to see you build something interesting, and be able to discuss it afterwards with us.

If you know React, feel free to simply create your application via [Create React App](https://github.com/facebook/create-react-app).

## Where do I get my data from?

It would be very difficult to create an API for this task in a short time, so we will use [Yelp's API for searching businesses](https://www.yelp.com/developers/documentation/v3/business_search) to retrieve our data. Yelp's API doesn't allow you to directly query it from the browser, so you could write a small NodeJS application to provide the data to your frontend, but luckily we've already done that part for you. 

Please refer to the documentation here: https://www.yelp.com/developers/documentation/v3/business_search. Our API uses the same query parameters as theirs.

For example, to search for any restaurant at a given ZIP code, run this in your terminal:

`curl https://cyxf2dye2m.execute-api.us-east-1.amazonaws.com/default/lunchyboys-search?location=32601`

If you're interested in deli's, try this:

`curl https://cyxf2dye2m.execute-api.us-east-1.amazonaws.com/default/lunchyboys-search?location=32601&term=delis`

You could even search for restaurants that have gender neutral bathrooms with:

`curl  https://cyxf2dye2m.execute-api.us-east-1.amazonaws.com/default/lunchyboys-search?location=32601&attributes=gender_neutral_restrooms`

## Requirements

The page should offer some inputs, including ZIP code, to help filter down restaurants around them. Feel free to allow the user to enter their ZIP from a text input.

Based on the inputs, there should be a single (random?) restaurant that is displayed on the screen.

The user should be able to click a button to regenerate a possible restaurant. If you have creative ways to handle this or any other requirement, get crazy!

In the end, as long as your application can generate a random restaurant based on your location, you've completed the minimal viable product. Where's the fun in that though? 

Feel free to get innovative with the application, including:
* What inputs the application provides to filter the restaurant
* Styling to make the application look fun!
* Interesting information about the business
* Any other ideas you can come up with

## Finishing the project

Once you are happy with your creation, send a link to your repo to michael@feathr.co and dev@feathr.co and we will take a look.

## Questions

If you have any questions or concerns, please email us at dev@feathr.co or michael@feathr.co.


